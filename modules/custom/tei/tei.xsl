<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" 
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="tei">    
    
    <xsl:output method="text"></xsl:output>
    <!-- <xsl:preserve-space elements="*"></xsl:preserve-space> -->
    <xsl:strip-space elements="*"></xsl:strip-space>
   
    <xsl:variable name='newline'>
        <xsl:text disable-output-escaping="yes">
</xsl:text>
    </xsl:variable>
    
    <!-- Suppress the content of teiHeader elements (used elsewhere): -->
    <xsl:template match="tei:teiHeader">
        <xsl:apply-templates/>
    </xsl:template>

<!-- START HEADER SECTION --> 
<!-- section relates to the header section only -->

    <!-- title -->
    <xsl:template match="tei:title">
        <xsl:text>Title: </xsl:text>
        <xsl:value-of select="//tei:title"/>
        <xsl:value-of select="$newline"/>

    </xsl:template>

<!-- supressed text elements in teiHeader -->
<xsl:template match="//tei:authority"/>
<xsl:template match="//tei:editor"/>

    <!-- Inscription ID -->
    <xsl:template match="//tei:idno[@type='siddham_inscription_identifier']">
        <xsl:text>Inscription ID: </xsl:text>
        <xsl:value-of select="//tei:idno[@type='siddham_inscription_identifier']"/>
      <xsl:value-of select="$newline"/>
    </xsl:template>
 
    <!-- Object decription (partial)  -->
    <xsl:template match="tei:sourceDesc">
<!-- <xsl:text>Source object: </xsl:text>
        <xsl:value-of select="./tei:msDesc/tei:msIdentifier/tei:msName"/>
        <xsl:value-of select="$newline"/>
 -->
 <!--
        <xsltext>Object ID: </xsltext>
        <xsl:value-of select="//tei:idno[@type='siddham_object_identifier']"/>
-->
<!--     <xsl:value-of select="$newline"/>
    <xsl:if test="string-length(./tei:msDesc/tei:msIdentifier/tei:repository &gt; 0)">
       <xsl:text>Current location: </xsl:text>
        <xsl:value-of select="./tei:msDesc/tei:msIdentifier/tei:repository"/>
 -->
        <!--       
     <xsl:value-of select="$newline"/>
-->
<!-- 
            </xsl:if>
-->
<!-- the rest of the object details are not required -->
    </xsl:template>
    
 <!-- end Object description -->
    
<!-- END HEADER SECTION -->
    
    <xsl:template match="tei:text/tei:body/div/*">
        <xsl:value-of select="normalize-space(.)"/>
        <xsl:apply-templates/>
    </xsl:template>
 
    <!-- Supress comments -->
    <xsl:template match="comment()"/>
    
    <!-- strip returns to prevent formatting errors -->
    <xsl:template match="text()">
        <!--  this is the test for "-<lb..." -->
        <xsl:value-of select="translate(translate(.,'|', '.'),'&#10;', '')"/>
        <xsl:apply-templates/>
      <xsl:call-template name="search-and-replace">
            <xsl:with-param name="input" select="text()"/>
            <xsl:with-param name="search-string" select="'-INSERT_NO_BREAK'"/>
            <xsl:with-param name="replace-string" select="''"/>
        </xsl:call-template>
    </xsl:template>
  
    <!-- line break -->
    <xsl:template match="tei:lb[@n]">            
        <xsl:choose>
            <xsl:when test="@break = 'no'">
                <xsl:value-of select="concat('XXXX', 'line ', ./@n, ': ', 'ZZZZ')"/>
                <xsl:apply-templates/>
            </xsl:when>
  <!-- when "-" at end of line -->          
            <xsl:when test="substring(preceding-sibling::text()[1], string-length(preceding-sibling::text()[1])-1, 1) = '-' and ./@break='no'">
                <xsl:text>INSERT_NO_BREAK</xsl:text>
            </xsl:when>
 <!-- when only a space after "-" at end of line -->
            <xsl:when test="(substring(preceding-sibling::text()[1], string-length(preceding-sibling::text()[1])-2, 1) = '-') and (substring(preceding-sibling::text()[1], string-length(preceding-sibling::text()[1])-1, 1) = ' ') and ./@break='no'">
               <xsl:text>INSERT_NO_BREAK</xsl:text>              
            </xsl:when>
            <xsl:when test="parent::tei:l or child::tei:l">
                <xsl:if test="not(./@break='no') and (./@n &gt; 0)">
                    <xsl:text>&#10;</xsl:text>
                    <xsl:value-of select="concat('line ', ./@n, ': ')"/>
                </xsl:if>
                <!-- ITS NOT CLEAR WHETHER TO BREAK @LINE-BREAKS WITHIN VERSE 
                <xsl:apply-templates/> -->
            </xsl:when>
            <xsl:otherwise>
                <!-- ignore lb if the @ value  is not a number,
                   eg is a margin -->
                <xsl:if test="string(number(@n)) != 'NaN'">
                    <xsl:text>&#10;</xsl:text>
                <xsl:value-of select="concat('line ', @n, ': ')"/>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
 
 <xsl:template match="tei:l">
      <xsl:text>&#10;</xsl:text>
     <xsl:apply-templates/>
 </xsl:template>
    
    <!-- supress surplus elements -->
    <xsl:template match="tei:surplus"/>
    
    <xsl:template match="tei:lg">
       <xsl:apply-templates/>
    </xsl:template>
    
    <!-- ignore paragraphs -->
    <xsl:template match="tei:p">
        <xsl:apply-templates/>
    </xsl:template>
    
    <!-- match and insert gaps -->
    <xsl:template match="tei:gap">
        <xsl:choose>
            <xsl:when test="@extent='unknown'">
                <xsl:text>...</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text> </xsl:text>
            </xsl:otherwise>
        </xsl:choose>

        <!--       <xsl:call-template name="add-spaces"> 
            <xsl:with-param name="quantity" select="@quantity"/>
        </xsl:call-template>
-->
    </xsl:template>
    
    <!-- choice element -->
    <!-- not complete - 
        rules - 1. prefer correction to any other tag
                2. choose first unlear over second unclear 
                3. (and not correct) add allchild nodes 
    -->
    <xsl:template match="tei:choice">
        <xsl:choose>
            <xsl:when test="./tei:corr">   
                <xsl:value-of select="tei:corr"/> 
            </xsl:when>
            <xsl:when test="count(./tei:unclear)= 2 ">
                <xsl:value-of select="./tei:unclear[1]"/>
            </xsl:when>
            <xsl:when test="./tei:unclear and ./tei:sic">
                <xsl:value-of select="./tei:*"/>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <!-- call this if you want to strip all extra spaces 
      <xsl:template match="text()">
          <xsl:value-of select="normalize-space(.)"/>
      </xsl:template>
  -->
    
    <!-- insert spaces as required -->  
    <xsl:template name="add-spaces">
        <xsl:param name ="quantity"/>
        <xsl:param name="i" select="1"/>
        <xsl:if test="not($i &gt; $quantity)">
            <xsl:text> </xsl:text>
            <xsl:call-template name="add-spaces">
                <xsl:with-param name="i" select="$i + 1" />
                <xsl:with-param name="quantity" select="$quantity"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="search-and-replace">
        <xsl:param name="input"/>
        <xsl:param name="search-string"/>
        <xsl:param name="replace-string"/>
        <xsl:choose>
            <!-- See if the input contains the search string -->
            <xsl:when test="$search-string and 
                contains($input,$search-string)">
          <!-- If the search string is found,
          concatenate the substring before the search
          string to the replacement string and to the result of
          recursively applying this template to the remaining substring.
          -->

                <xsl:value-of 
                    select="substring-before($input,$search-string)"/>
                <xsl:value-of select="$replace-string"/>                
                <xsl:call-template name="search-and-replace">
                    <xsl:with-param name="input"
                        select="substring-after($input,$search-string)"/>
                    <xsl:with-param name="search" select="$search-string"/>
                    <xsl:with-param name="replace" select="$replace-string"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <!-- There are no more occurences of the search string so 
               just return the current input string -->
                <xsl:value-of select="$input"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
 
   <xsl:template name="insert_line_num">
       <xsl:param name="input"/>
           <xsl:choose>
               <xsl:when test="string-length(substring-before($input, '-')) &lt; string-length(substring-before($input, ' '))">
                   <xsl:value-of select="concat(substring-before($input, '-'),'
', substring-after($input, '-'))"/>
               </xsl:when>
               <xsl:when test="string-length(substring-before($input, ' ')) &lt; string-length(substring-before($input, '-'))">
                   <xsl:value-of select="concat(substring-before($input, ' '),'
                       ', substring-after($input, ' '))"/>  
               </xsl:when>
               <xsl:when test="substring($input, ' ')">
                   <xsl:value-of select="concat(substring-before($input, ' '),'
                   ', substring-after($input, ' '))"/>
               </xsl:when>
               <xsl:when test="substring($input, '-')">
                   <xsl:value-of select="concat(substring-before($input, '-'),'
                       ', substring-after($input, '-'))"/>
               </xsl:when>
           </xsl:choose>

   </xsl:template>
</xsl:stylesheet>