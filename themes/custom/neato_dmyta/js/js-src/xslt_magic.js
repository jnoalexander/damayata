(function ($, Drupal) {



    Drupal.behaviors.xslt_magic = {

        attach: function (context, settings) {



			$(function() {



				// The following implements "magicXML" from:

				// http://tomdavies.azurewebsites.net/magicxml/



				var xsl_path = ("/themes/custom/neato_dmyta/js/js-src/transform.xsl");

				$('#block-neato-dmyta-content .field--name-display-field-copynode-xml-plain-xslted .field__item').attr( 'data-xslt', xsl_path);

				//alert(xsl_path);

				// alert($('#block-neato-dmyta-content .field--name-field-xml-plain .field__item').data('xsl'));


               //var xml_path = ("/themes/custom/neato_dmyta/js/js-src/test3.xml");
               
				var xml_text = $('#block-neato-dmyta-content .field--name-display-field-copynode-xml-plain-xslted .field__item').text();

				// console.log(xml_text);

				//xml_text_loaded = ("NodeXML.xml");



				$('#block-neato-dmyta-content .field--name-display-field-copynode-xml-plain-xslted .field__item').attr( "data-xml", xml_text);

				//console.log(xml_set);

				//alert($('#block-neato-dmyta-content .field--name-field-xml-plain .field__item').data('xml'));

                $('#block-neato-dmyta-content .field--name-display-field-copynode-xml-plain-xslted .field__item').text("");

				magicXML.parse('#block-neato-dmyta-content .field--name-display-field-copynode-xml-plain-xslted .field__item');


                 //console.log($('#block-neato-dmyta-content .field--name-display-field-copynode-xml-plain-xslted .field__item').text());


			});







        }

    };



})(jQuery, Drupal);