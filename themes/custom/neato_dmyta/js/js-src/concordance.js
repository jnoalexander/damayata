(function ($, Drupal) {



    Drupal.behaviors.concordance = {



        attach: function (context, settings) {



            var conc_ref = $('.field--name-field-concordance-item > div > a').attr('href');



            //console.log(conc_ref);







            var conc_array = '{"agrawala_1983": {"type": "book", "author": "Agrawala, Prithvi Kumar", "publication_date": "1983", "title": "Imperial Gupta Epigraphs (गुप्ताधिराजलेखमण्डल)", "publication_place": "Varanasi", "publisher": "Books Asia", "parent_ref": "", "pages": ""}, "bhandarkar_1929": {"type": "book", "author": "Bhandarkar, Devadatta Ramakrishna", "publication_date": "1929", "title": "A List of the Inscriptions of Northern India in Brahmi and its derivative Scripts, from about 200 A. C. (Appendix to Epigraphia Indica volumes XIX to XXIII)", "publication_place": "Calcutta", "publisher": "University of Calcutta", "parent_ref": "", "pages": ""}, "bhandarkar_1981": {"type": "book", "author": "Fleet, John Faithfull * Bhandarkar, Devadatta Ramakrishna", "publication_date": "1981", "title": "Inscriptions of the Early Gupta Kings (revised edition)", "publication_place": "New Delhi", "publisher": "Archaeological Survey of India", "parent_ref": "", "pages": ""}, "fleet_1888": {"type": "book", "author": "Fleet, John Faithfull", "publication_date": "1888", "title": "Inscriptions of the Early Gupta Kings and Their Successors", "publication_place": "Calcutta", "publisher": "Superintendent of Government Printing", "parent_ref": "", "pages": ""}, "goyal_1993": {"type": "book", "author": "Goyal, Shri Ram", "publication_date": "1993", "title": "गुप्तकालीन अभिलेख/Guptakālīna Abhilekha (Inscriptions of the Gupta Age)", "publication_place": "Jodhpur", "publisher": "Kusumanjali Prakashan", "parent_ref": "", "pages": ""}, "mirashi_1963": {"type": "book", "author": "Mirashi, Vasudev Vishnu", "publication_date": "1963", "title": "Inscriptions of the Vākāṭakas", "publication_place": "Ootacamund", "publisher": "Archaeological Survey of India", "parent_ref": "", "pages": ""}, "sircar_1965": {"type": "book", "author": "Sircar, Dines Chandra", "publication_date": "1965", "title": "Select Inscriptions Bearing on Indian History and Civilization. Volume I: From the Sixth Century B.C. to the Sixth Century A.D. (Second Edition)", "publication_place": "Calcutta", "publisher": "University of Calcutta", "parent_ref": "", "pages": ""}, "thaplyal_1985": {"type": "book", "author": "Thaplyal, Kiran Kumar", "publication_date": "1985", "title": "Inscriptions of the Maukharīs, Later Guptas, Puṣpabhūtis and Yaśovarman of Kanauj", "publication_place": "New Delhi", "publisher": "Indian Council of Historical Research", "parent_ref": "", "pages": ""}, "tsukamoto_1996": {"type": "book", "author": "Tsukamoto, Keisho", "publication_date": "1996", "title": "A Comprehensive Study of the Indian Buddhist Inscriptions. Part I. Text, Notes and Japanese Translation", "publication_place": "Kyoto", "publisher": "Heirakuji-Shoten", "parent_ref": "", "pages": ""}}';



            var concs = JSON.parse(conc_array);







            $('.field--name-field-concordance-item > div > a').each(function(){



                    //console.log($(this).attr('href'))



                var url = $(this).attr('href');    



                var name = url.replace('/bibliography/', '');

                

              // console.log(match);          

             var ref = '';
             
             if(concs[name].author.length > 0) {

                 ref += concs[name].author;

                 ref += '. (';

               }

             if(concs[name].publication_date.length > 0) {
             
                 ref += concs[name].publication_date;
            
               }
            
                 ref += ').\n';

                 ref += concs[name].title;

                 ref += '.\n';

             if(concs[name].publication_place.length > 0) {
             
                 ref += concs[name].publication_place;

                 ref +=': ';
                 
               }

             if(concs[name].publisher.length > 0) {

                 ref += concs[name].publisher;

                 ref += '.'; 
              }


                    $(this).attr('data-tooltip', ref);
                    
                    // took this out, I don't know why
                    // $(this).text($(this).text().replace("_", " "));

                    //console.log($(this).text());

                //    $(this).attr('title', 'Fleet, John Faithfull. (1888). \nInscriptions of the Early Gupta Kings and Their Successors.\nCalcutta: Superintendent of Government Printing.');



            });



// function ended



        }



    };



})(jQuery, Drupal);



















