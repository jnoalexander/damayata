(function ($, Drupal) {



    Drupal.behaviors.format_bib_ref = {



        attach: function (context, settings) {

        // get the raw JSON text and parse it 
        // $.getJSON('people.json', function(data) {
        $.getJSON( "/themes/custom/neato_dmyta/js/js-src/test_include.js", function( data ) {
        //var jrefs = data;
        //var refs = JSON.parse(data);
        var refs = data;
        //console.log(refs);
        
        // jquery selector(s) now an array
        var selector_array = [".field--name-field-biblliography-description > .field__item",".group-header > .field--name-body > p", ".field--name-field-authority-2 > .field__item", ".field--name-body > .field__item > p", ".left-inner-right > .field--name-field-place-and-date > .field__item", ".left-inner-left > .field--name-field-hand > .field__item"];
        
        for(var i in selector_array){
   
		  var selector = selector_array[i];

         if ($(selector).length ) {
                				 
          var bib_field = $(selector).text();

          var find_bibs = /\#([A-z0-9üāöèēś_-]+)/g;

          if(bib_field ){

            if(bib_field.match(find_bibs)){
          
            bib_field.match(find_bibs).forEach(function(element,index){

            var name = element.substring(1);        

            if(typeof refs[name] !== 'undefined'){

                var ref = '';

                ref += 'Reference: ';

                ref += refs[name].type;

                ref += '\n';

                if(refs[name].author.length > 0){
                
                    ref += refs[name].author;
                    
                    ref += '. ';
                                    
                }
                
				if(refs[name].publication_date.length > 0){
					ref += '(';

					ref += refs[name].publication_date;

					ref += ').';
								  
				}


                if((refs[name].author.length > 0) || (refs[name].publication_date.length > 0)){
                    ref += '\n';
                }
                
                if(refs[name].title.length > 0){
                    ref += refs[name].title;

                    ref += '.\n';
                }
                
                if(refs[name].publication_place.length > 0){   
                             
                    ref += refs[name].publication_place;

                    ref +=': ';
                
                }
                
                ref += refs[name].publisher;

                if(refs[name].type == 'book'){
                
                    if(refs[name].publisher.length > 0){
                    
                       ref += '.'; 
                    }
                    
                }else{
                
// if an article or chapter 
// get the name of the parent publication -

                   if(refs[name].parent_ref!=''){
                   
                       if(refs[name].publisher.length > 0){
                       
                           ref += ', ';
                       }

                       //ref += refs[name].parent_ref;
                       
                      var parent_title = refs[name].parent_ref;
                       
                       if(refs[parent_title].title.length > 0){
                         
                         ref += '\n';
                         
                         ref += refs[parent_title].title ;
                         
                         ref += '.\n';
                    
// if there is an issue number for a parent item, show this
                         
						if(refs[name].publication_date = typeof variable !== typeof undefined){
							 
							 if(refs[name].publication_date.length > 0){
                         
                                 ref += '(';
                         
                                 ref += refs[name].publication_date;
                         
                                 ref += '). ';
						
							 }
						}

// this is only set for articles where the pub date != issue date
							//if(refs[name].vol_issue_date = typeof variable !== typeof undefined){
						
								if(refs[name].vol_issue_date.length > 0){
								
									ref += 'Vol. publication date: ';
								
									ref += refs[name].vol_issue_date;
								
									ref += '. ';
						
								}
						//	}
                         
                       }
                    
                    }
                    
						if(refs[name].vol_issue.length > 0){   
					
							ref +=' Issue: ';
												 
							ref += refs[name].vol_issue;

							ref +='. ';
									
						}
                    
                    if(refs[name].pages!=''){

                       ref += '\n pp ';

                       ref += refs[name].pages;

                       ref += '.';

                    }
                    
                }

            }else{

                var ref = '';  

            }

         var a_title = ref;

         var final_ref = '<a';

         final_ref += ' href="/bibliography/';

         final_ref += element.substring(1);

         final_ref += '" data-tooltip="';

         final_ref += a_title;

         final_ref += '">';

		 final_ref += name.replace("_", " ");
         
         final_ref += '</a>';

          //console.log(final_ref);

         bib_field = bib_field.replace(element, final_ref);
         
         //bib_field=bib_field.replace(/\n/g,"<br>");

        });
        
         $(selector).html(bib_field);
        
        }
        
        }
         
      }
      
    }

    });
    
// end bib script 



        }



    };



})(jQuery, Drupal);