// (function ($) {
//   'use strict';

//   Drupal.behaviors.xlsToggle = {
//     attach: function(context, settings) {

//         $('#block-neato-dmyta-content .field--name-field-edition').find('#main').removeClass('xsl-dip');
//         $('#block-neato-dmyta-content .field--name-field-edition').find('#main').addClass('xsl-edition');

//     }
//   };

// }(jQuery));



(function ($, Drupal) {

    Drupal.behaviors.xsltToggle = {
        attach: function (context, settings) {

        $('.field--name-field-edition-toggle .edition-toggle').click(function() {
    	   $('#block-neato-dmyta-content .field--name-display-field-copynode-xml-plain-xslted #main').toggleClass('xsl-edition xsl-dip');
    	    //$('#block-neato-dmyta-content .field--name-field-edition').find('#main').addClass('xsl-edition');

                if ( $('#block-neato-dmyta-content .field--name-display-field-copynode-xml-plain-xslted #main').hasClass("xsl-edition") ) {
                    $('.field--name-field-edition-toggle .diplomatic-toggle').text('Critical Edition');
                }
                if ( $('#block-neato-dmyta-content .field--name-display-field-copynode-xml-plain-xslted #main').hasClass("xsl-dip") ) {
                    $('.field--name-field-edition-toggle .diplomatic-toggle').text('Diplomatic Edition');
                }

            });
        }
    };


    Drupal.behaviors.toggleLogin = {
      attach: function(context) {

        $("#manage-strip #block-userlogin h2.block-title").click(function () {


          $("#manage-strip #block-userlogin .user-login-form").slideToggle("slow");
          //$(".path-frontpage #manage-strip #block-userlogin h2.block-title").toggleClass('collapsed');
          });

        }
    };



    Drupal.behaviors.inscriptionDisplayToggle = {
        attach: function (context, settings) {

        $('.field--name-field-edition-toggle .display-toggle').click(function() {
           
                $('body').parent().toggleClass('page-node-type-inscription-wide');

            });        

        }

    };
    

    Drupal.behaviors.appendURI = {
        attach: function (context, settings) {

        

        $('.field--name-field-object-id > .field__item').each(function(){ 

            var item = '/object/';
            
            var my_url = '<a href="';
            my_url += 'http://';
            my_url += document.location.hostname;
            my_url += item;
            my_url += $( this ).text();
            my_url += '">http://';
            my_url += document.location.hostname;
            my_url += item;
            my_url += $( this).text();
            my_url += '</a>';

        $(this).html( my_url );
        });


        $('.field--name-field-inscription-id > .field__item').each(function(){ 

            var item = '/inscription/';

            var my_url = '<a href="';
            my_url += 'http://';
            my_url += document.location.hostname;
            my_url += item;
            my_url += $( this ).text();
            my_url += '">http://';
            my_url += document.location.hostname;
            my_url += item;
            my_url += $( this).text();
            my_url += '</a>';

        $(this).html( my_url );
        });

        // $('#block-neato-dmyta-content .field--name-field-object-id .field__item').prepend('http://', $(location).attr('hostname'), '/object/');

        // $('#block-neato-dmyta-content .field--name-field-inscription-id .field__item').prepend('http://', $(location).attr('hostname'), '/inscription/');

        }
    };
  
})(jQuery, Drupal);