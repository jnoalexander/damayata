(function ($, Drupal) {

    Drupal.behaviors.ob_and_in_links = {

        attach: function (context, settings) {
        
		    var selector_array = [".group-header > .field--name-body > p", ".field--name-field-authority-2 > .field__item", ".field--name-body > .field__item > p", ".left-inner-right > .field--name-field-place-and-date > .field__item", ".left-inner-left > .field--name-field-hand > .field__item", ".details-wrapper > .field--name-field-biblliography-description > .field__item", ".top-wrapper > .field--name-field-inscription-description"];

			for(var i in selector_array){
      
				var selector = selector_array[i];            

                if ($(selector).length ) {

					var active_field = $(selector).text();
			
					//var active_field = $('.field--name-body > p').text();

					//console.log("Selecting "+ selector);
		  
					var match_text = /\%((O|I)-(\d+)|(((IN)|(OB))\w+))/g;

                    if(active_field.match(match_text))
                    {
						active_field.match(match_text).forEach(function(element,index){
                          
                          console.log("element is " + element);
                          
						  if(element.substring(1,3) == "OB") {
							var type = "object";
                            var prefix = "OB";
                            var start = 3;
                          }else if(element.substring(1,2) == "O"){
                            var type = "object";
                            var prefix = "OB0";
                             var start = 3;
                          }else if(element.substring(1,2) == "I"){
						    var type = "inscription";
                            var prefix = "IN0";
                             var start = 3;
                          }else{
							var type = "inscription";
                            var prefix = "IN";
                             var start = 3;
						  }
                         // console.log("My type is " + type);
					  
						  var final_text = '<a';
							  final_text += ' href="/';
							  final_text += type;
							  final_text += '/';
                              final_text += prefix;
							  final_text += element.substring(start);
							  final_text += '" title="';
							  final_text += 'view ';
							  final_text += type + ' ';
                              final_text += prefix;
							  final_text += element.substring(start)
							  final_text += '">';
							  final_text += type;
							  final_text +=' ';
                              final_text += prefix;
							  final_text += element.substring(start);
							  final_text += '</a>';
                              
                            //  console.log(final_text);    
                          
							  active_field = active_field.replace(element, final_text)
						  //console.log(active_field);

							  $(selector).html(active_field);

						});
					}

				}
			}
		}
    };
})(jQuery, Drupal);
