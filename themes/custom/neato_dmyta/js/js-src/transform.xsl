<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="tei">    
<!-- This stylesheet is used for displaying object overview page -->
<xsl:output method="html" encoding="UTF-8" omit-xml-declaration="yes"></xsl:output>
<!-- <xsl:preserve-space elements="*"></xsl:preserve-space> -->
<xsl:strip-space elements="*"></xsl:strip-space>

<!-- add newline for formating -->
<xsl:variable name='newline'><xsl:text disable-output-escaping="yes">&lt;br /&gt;</xsl:text></xsl:variable>
    <!-- CHANGES FOR FIREFOX BELOW 
<xsl:variable name='newline_dip'><xsl:text disable-output-escaping="yes">&lt;br class=&quot;xsl-dip-only&quot; /&gt;</xsl:text> </xsl:variable> 
<xsl:variable name='newline_edition'><xsl:text disable-output-escaping="yes">&lt;br class=&quot;xsl-edition-only&quot; /&gt;</xsl:text></xsl:variable> 
    -->
    <xsl:variable name='newline_dip'><div class="xsl-dip-only"></div></xsl:variable> 
    <xsl:variable name='newline_edition'><div class="xsl-edition-only"></div></xsl:variable> 
    
<xsl:variable name='oa_bracket'><xsl:text disable-output-escaping="yes">&#0060;</xsl:text></xsl:variable> 
<xsl:variable name='ca_bracket'><xsl:text disable-output-escaping="yes">&#0062;</xsl:text></xsl:variable> 

<!-- for encoding problems  could try &amp;#x -->
<xsl:variable name='met_brev'><xsl:text>&#x23d1;</xsl:text></xsl:variable>
<xsl:variable name='short_over_long'><xsl:text>&#x23D3;</xsl:text></xsl:variable>    
<xsl:variable name='edspace'><xsl:element name="span"><xsl:attribute name="class">xsl-edition-only</xsl:attribute><xsl:text>-</xsl:text></xsl:element> </xsl:variable>   

<!-- Suppress the content of head and teiHeader elements (used elsewhere): -->
<xsl:template match="head">
<!--     <xsl:element name="meta">
<xsl:attribute name="charset">UTF-8</xsl:attribute>
</xsl:element>
-->
</xsl:template>

<xsl:template match="tei:teiHeader"/>
<xsl:template match="//tei:body"><!-- html5 
<xsl:element name="html5"> -->
<div type="edition">
<xsl:attribute name="class">xsl-dip</xsl:attribute>
<xsl:attribute name="id">main</xsl:attribute>
<xsl:attribute name="name">main</xsl:attribute>
<xsl:apply-templates/> 
</div>
<!-- end html5 
</xsl:element>  
-->
</xsl:template> 

<xsl:template match="tei:div[@type='textpart']">

        <xsl:element name="div">

            <xsl:attribute name="class">xsl-textpart</xsl:attribute>

        <xsl:element name="h2">

            <xsl:attribute name="title">

                <xsl:value-of select="concat('text part = ', ./@subtype)"/>

            </xsl:attribute>

            <xsl:value-of select="./@subtype"/>

            <xsl:if test="./@n">

                <xsl:text> </xsl:text>

                <xsl:value-of select="./@n"/>

            </xsl:if>

        </xsl:element>

 <!--           <xsl:for-each select="./tei:p">

                <xsl:call-template name="para">

                    <xsl:with-param name="count" select="position()"/>

                </xsl:call-template>

            </xsl:for-each>

-->    

            <xsl:apply-templates/>

        </xsl:element>

    </xsl:template>

    

    <xsl:template match="//tei:div[1]/tei:p[1]/tei:pb[1]">  

        <xsl:element name="span">

            <xsl:attribute name="class">xsl-pb</xsl:attribute>

            <xsl:copy-of select="$oa_bracket"/>

            <xsl:if test="./@n">

                <xsl:text>Page </xsl:text>

            </xsl:if>

            <xsl:choose>

                <xsl:when test="contains(./@n, 'verso')">

                    <xsl:value-of select="concat(substring-before(./@n, 'verso'), ' verso')"/>

                </xsl:when>

                <xsl:when test="contains(./@n, 'recto')">

                    <xsl:value-of select="concat(substring-before(./@n, 'recto'), ' recto')"/>

                </xsl:when>

                <xsl:otherwise>

                    <xsl:value-of select="./@n"/>

                </xsl:otherwise>

            </xsl:choose>

            <xsl:copy-of select="$ca_bracket"/>

        </xsl:element>

 <!--       <xsl:copy-of select="$newline_dip"/> -->

      

    </xsl:template>

    

<!-- pb and lb both take css style "greyout" -->

<!-- page break -->

    <xsl:template match="tei:pb"> 

        <xsl:if test="string-length(./@n) &gt; 0 and ./@n &gt; 1">

        <xsl:copy-of select="$newline_dip"/>

        </xsl:if>

        <xsl:copy-of select="$newline_dip"/>

            <xsl:element name="span">

                <xsl:attribute name="class">xsl-pb</xsl:attribute>

                <xsl:copy-of select="$oa_bracket"/>

                <xsl:if test="./@n">

   

            <xsl:text>Page </xsl:text>

        </xsl:if>

                <xsl:choose>

                    <xsl:when test="contains(./@n, 'verso')">

                        <xsl:value-of select="concat(substring-before(./@n, 'verso'), ' verso')"/>

                    </xsl:when>

                    <xsl:when test="contains(./@n, 'recto')">

                        <xsl:value-of select="concat(substring-before(./@n, 'recto'), ' recto')"/>

                    </xsl:when>

                    <xsl:otherwise>

                        <xsl:value-of select="./@n"/>

                    </xsl:otherwise>

                </xsl:choose>

            <xsl:copy-of select="$ca_bracket"/>

        </xsl:element>

  <!--      <xsl:copy-of select="$newline_dip"/>  -->

    </xsl:template>

  

<!-- line break - make newline and show line number -->  

    <xsl:template match="tei:lb">

        <xsl:if test="./@break='no'">

            <xsl:element name="span"><xsl:attribute name="class">xsl-no-break</xsl:attribute><xsl:text>&#x2D;</xsl:text>

            </xsl:element> 

        </xsl:if>

        <xsl:copy-of select="$newline_dip"/>

 

 <!-- two different styles for edition and dip as

     only showing "<...>" for critical edition -->

       <xsl:element name="span">

            <xsl:attribute name="class">xsl-greyout xsl-line-break-edition xsl-edition-only</xsl:attribute>

           <xsl:if test="./@n"> <xsl:copy-of select="$oa_bracket"/><xsl:value-of select="./@n"/> <xsl:copy-of select="$ca_bracket"/></xsl:if>

        </xsl:element> 

        <xsl:element name="span">

            <xsl:attribute name="class">xsl-greyout xsl-line-break-dip xsl-dip-only</xsl:attribute>

            <xsl:if test="./@n"><xsl:value-of select="./@n"/></xsl:if>

        </xsl:element>

        <xsl:apply-templates/>

    </xsl:template>

    

    <!-- space with x2 lines... for p, ab and lg -->

    <xsl:template match="//tei:body/tei:div[1]/tei:p[1]">

        <xsl:element name="span">

            <xsl:attribute name="class">xsl-greyout xsl-paragraph</xsl:attribute>

     <!-- supress paragraph label    

         <xsl:value-of select="concat('Paragraph ', '1')"/>

     -->

        </xsl:element>

        <xsl:apply-templates/>

    </xsl:template>

    <xsl:template match="//tei:body/tei:div[1]/tei:div/tei:p[1]">

        <xsl:element name="span">

            <xsl:attribute name="class">xsl-greyout xsl-paragraph</xsl:attribute>

            <!-- supress paragraph label    

                <xsl:value-of select="concat('Paragraph ', '1')"/>

             -->

        </xsl:element>

        <xsl:apply-templates/>

    </xsl:template>

    

    <!-- paragraph -->

    <xsl:template match="//tei:p[position() &gt; 1]"> 

        <xsl:copy-of select="$newline_edition"/>

        <xsl:copy-of select="$newline_edition"/> 

        <xsl:element name="span">

            <xsl:attribute name="class">xsl-greyout xsl-paragraph</xsl:attribute>

            <!-- supress paragraph label    

                <xsl:value-of select="concat('Paragraph ', '')"/><xsl:number/>

   -->

        </xsl:element>

            <xsl:apply-templates/>

    </xsl:template>

    

    <!-- annonymous block -->

    <xsl:template match="tei:ab">

        <!-- only add breaks if not first element -->

        <xsl:if test="preceding-sibling::div">

            <xsl:copy-of select="$newline_edition"/>

            <xsl:copy-of select="$newline_edition"/>

        </xsl:if>

            <xsl:apply-templates/>

    </xsl:template>    

    

    <!-- line group -->

    <xsl:template match="tei:lg">

        <xsl:if test="preceding-sibling::tei:lg or preceding-sibling::tei:p">

        <xsl:copy-of select="$newline_edition"/>

        <xsl:copy-of select="$newline_edition"/>

        </xsl:if>       

        <xsl:element name="span">

            <xsl:attribute name="class">xsl-lg-label</xsl:attribute>

        <xsl:text>Verse </xsl:text>

            <xsl:value-of select="./@n"/>

            <xsl:text>. </xsl:text>

            <xsl:if test="./@met">

                <xsl:text>(metre: </xsl:text>

            <xsl:value-of select="./@met"/>

                <xsl:text>)</xsl:text>

            </xsl:if>

        </xsl:element>

        <xsl:apply-templates/>

    </xsl:template>

    

    <!-- ignore lg@type="..." element -->

    <xsl:template match="tei:lg[@type]">

        <xsl:apply-templates/>

    </xsl:template>

    

    <xsl:template match="tei:l">

            <xsl:copy-of select="$newline_edition"/>

     <xsl:apply-templates/>

    </xsl:template>

    <!--- –	symbols that are neither alphabetic, nor numerical, 

        nor punctuation marks, are shown as "g" elements -->

    <xsl:template match="tei:g">

        <xsl:element name="span">

            <xsl:attribute name="class">xsl-generic-symbol</xsl:attribute>

        <xsl:text>&#8224;</xsl:text>

         <!-- start comment

             to do -  display these differenty with @type 

             and @ subtype (when this is added), something like -

         <xsl:choose>

             <xsl:when test="./@type=''">

                 <xsl:text></xsl:text>

             </xsl:when>

             <xsl:when test="./@type=''"></xsl:when>

         </xsl:choose>

       end  comment -->

        </xsl:element>

        <xsl:apply-templates/>

    </xsl:template>

    

    <!-- rules for displaying spaces '_' or _n_ for multiple or for lines -->

    <xsl:template match="tei:space">

        <xsl:choose>

            <xsl:when test="./@unit='character' and ./@quantity='1'">

                <xsl:element name="span">

                    <xsl:attribute name="class">xsl-space-char</xsl:attribute>

                    <xsl:attribute name="title">

                        <xsl:value-of select="concat(./@quantity, ' space here')"/>

                    </xsl:attribute>

                    <xsl:text>_</xsl:text>

                </xsl:element>

            </xsl:when>

            <xsl:when test="./@unit='character' and ./@quantity&gt;'1'"> 

                <xsl:element name="span">

                    <xsl:attribute name="class">xsl-space-chars</xsl:attribute>

                    <xsl:attribute name="title">

                        <xsl:value-of select="concat(./@quantity, ' spaces here')"/>

                    </xsl:attribute>

                        <xsl:value-of select="concat('_',./@quantity,'_')"/>

                </xsl:element>

            </xsl:when>

            <xsl:when test="./@unit='line'">

                <!-- loop of n line breaks -->

                <xsl:for-each select="./@quantity">

                <xsl:element name="span">

                    <xsl:attribute name="class">xsl-space-line</xsl:attribute>

                    <xsl:attribute name="title">

                        <xsl:value-of select="'line space'"/>

                    </xsl:attribute>           

                    <xsl:element name="br"></xsl:element>                

                    <xsl:value-of select="'---'"/>

                </xsl:element>

                    <xsl:element name="br"></xsl:element>

                </xsl:for-each>

            </xsl:when>

        </xsl:choose>

        <xsl:apply-templates/>

    </xsl:template>

 

<!-- for repeating values -->

    <xsl:template name="iterate">

        <xsl:param name="i" select="1" />

        <xsl:param name="count" />

        <xsl:param name="char_type" select="'-'"/>

        

        <xsl:if test="$i &lt;= $count">

                <xsl:value-of select="$char_type"/>

                <xsl:apply-templates select="iterate" />

        </xsl:if>

        

        <!--begin_: Repeat Loop Until Finished $count -->

        <xsl:if test="$i &lt;= $count">

            <xsl:call-template name="iterate">

                <xsl:with-param name="i">

                    <xsl:value-of select="$i + 1"/>

                </xsl:with-param>

                <xsl:with-param name="count">

                    <xsl:value-of select="$count"/>

                </xsl:with-param>

                <xsl:with-param name="char_type">

                    <xsl:value-of disable-output-escaping="yes" select="$char_type"/>

                </xsl:with-param>

            </xsl:call-template>

        </xsl:if>       

    </xsl:template>

      <xsl:template match="tei:gap">

          <xsl:element name="span">

              <xsl:attribute name="class">xsl-gap</xsl:attribute>

              <xsl:attribute name="title">

                  <xsl:value-of select="'gap'"/>

              </xsl:attribute>

              <xsl:text>[</xsl:text>

  <!--      <xsl:text>DEBUG GAP</xsl:text> -->

        <xsl:choose>

            <xsl:when test="./@reason='lost'">

                <xsl:call-template name="determine_char">

                    <xsl:with-param name="char" select="'*'"/>

                    <xsl:with-param name="quantity">

                        <xsl:value-of select="./@quantity"/>

                    </xsl:with-param>

                    <xsl:with-param name="extent">

                        <xsl:value-of select="./@extent"/>

                    </xsl:with-param>

                    <xsl:with-param name="unit">

                        <xsl:value-of select="./@unit"/>

                    </xsl:with-param>

                    <xsl:with-param name="precision">

                        <xsl:value-of select="./@precision"/>

                    </xsl:with-param>

                </xsl:call-template>

            </xsl:when>

            <xsl:when test="./@reason='illegible'">

                <xsl:call-template name="determine_char">

                    <xsl:with-param name="char" select="'#'"/>

                    <xsl:with-param name="quantity">

                        <xsl:value-of select="./@quantity"/>

                    </xsl:with-param>

                    <xsl:with-param name="extent">

                        <xsl:value-of select="./@extent"/>

                    </xsl:with-param>

                    <xsl:with-param name="unit">

                        <xsl:value-of select="./@unit"/>

                    </xsl:with-param>

                    <xsl:with-param name="precision">

                        <xsl:value-of select="./@precision"/>

                    </xsl:with-param>

                </xsl:call-template>

            </xsl:when>

            

        </xsl:choose>

          <!-- do stuff if there's a seg met parent -->

              <xsl:text>]</xsl:text>

          </xsl:element>

          <xsl:apply-templates/>

      </xsl:template>

 

 <!-- 2.4 template for seg @met translation of characters   

 -->

    

    <xsl:template match="tei:seg[@met]">

        <xsl:element name="span">

            <xsl:attribute name="class">xsl-seg</xsl:attribute>

            <xsl:attribute name="title">

                <xsl:value-of select="./gap/@reason"/>

            </xsl:attribute>

            <xsl:text>[</xsl:text>

            <!-- add meter here -->

            <xsl:call-template name="seg-met-convert">

                <xsl:with-param name="meter" select="./@met"/>

             </xsl:call-template>

            <!-- end call to met convertor -->

            <xsl:text>]</xsl:text>

        </xsl:element>

    </xsl:template>

 

    <!-- ignore the gap after a seg@met element as it has been dealt with -->

    <xsl:template match="//tei:seg[@met]/tei:gap">

    </xsl:template>

    

 <!-- 3.1 text -->

    <xsl:template match="tei:unclear">

        <xsl:element name="span">

            <xsl:attribute name="class">xsl-unclear</xsl:attribute>

            <xsl:attribute name="title">

                <xsl:value-of select="'unclear'"/>

            </xsl:attribute>

        <xsl:text>(</xsl:text>

        <xsl:if test="./@cert='low'">

            <xsl:text>?</xsl:text>

        </xsl:if>

        <xsl:value-of select="concat(., ')')"/>

        <!-- supress element 

        <xsl:apply-templates/>

        -->

        </xsl:element>

    </xsl:template>

<!-- 3.2 num -->

    <xsl:template match="tei:num">

          <xsl:element name="span">

              <xsl:attribute name="class">xsl-highlight-number</xsl:attribute>

              <xsl:attribute name="title">

                  <xsl:value-of select="concat('numeral: ', .)"/>

              </xsl:attribute>

              <xsl:value-of select="."/>

          </xsl:element>

     <!-- do not call element template 

         <xsl:apply-templates/>

        -->

    </xsl:template>

<!-- 3.3 del {xabc} -->

    <xsl:template match="tei:del">

        <xsl:element name="span">

            <xsl:attribute name="class">xsl-deletion</xsl:attribute>

        <xsl:text>{&#735;</xsl:text>

        <xsl:value-of select="concat(., '}')"/>

        </xsl:element>

      <!--  do not apply templates or text wil be repeated -->

    </xsl:template>

<!-- 3.4 add {+abc} -->

    <xsl:template match="tei:add">

        <xsl:element name="span">

            <xsl:attribute name="class">xsl-add</xsl:attribute>

            <xsl:if test="./@place">

            <xsl:attribute name="title">

                <xsl:value-of select="./@place"/>

            </xsl:attribute>

            </xsl:if>

        <xsl:text>{+</xsl:text>

        <xsl:value-of select="concat(., '}')"/>

        </xsl:element>

        <!--  do not apply templates or text wil be repeated -->

    </xsl:template>

<!-- 3.5 surplus {-abc} --> 

    <xsl:template match="tei:surplus">

        <xsl:element name="span">

            <xsl:attribute name="class">xsl-surplus</xsl:attribute>

        <xsl:value-of select="concat('{-', ., '}')"/>  

        </xsl:element>

        <!--  do not apply templates or text wil be repeated -->

    </xsl:template>   

    

<!-- 3.6 sic display in red  -->

    <xsl:template match="tei:sic">

        <xsl:element name="span">

            <xsl:attribute name="class">xsl-sic</xsl:attribute>

        <xsl:value-of select="."/>

         

        </xsl:element>

     <!-- do not apply additional templates -->

    </xsl:template>

 

 <!-- 3.7 supplied --> 

    <xsl:template match="tei:supplied[@reason= ('lost' or 'illegible' or 'undefined')]">

        <xsl:element name="span">

            <xsl:attribute name="class">xsl-supplied</xsl:attribute>

            <xsl:attribute name="title">

                <xsl:value-of select="concat('supplied reason ', @reason)"/>

            </xsl:attribute>

            <xsl:text>[</xsl:text>

        <xsl:choose>

            <xsl:when test="./@reason='lost'">

                <xsl:text>*</xsl:text>

            </xsl:when>

            <xsl:when test="./@reason='illegible'">

                <xsl:text>#</xsl:text>

            </xsl:when>

            <xsl:when test="./@reason='undefined'">

                <!-- for dip view the symbol is &#126; -->

                <xsl:text></xsl:text>

            </xsl:when>

        </xsl:choose>

        <xsl:if test="./@cert='low'">

            <xsl:text>?</xsl:text>

        </xsl:if>

                 <xsl:value-of select="(.)"/>

                <xsl:text>]</xsl:text>          

        </xsl:element>

        <!-- do not call element template -->

       

    </xsl:template>  

    <xsl:template match="tei:supplied[@reason='omitted']">

        <xsl:element name="span">

            <xsl:attribute name="class">xsl-supplied  xsl-edition-only</xsl:attribute>

            <xsl:attribute name="title">

                <xsl:value-of select="concat('supplied reason ', @reason)"/>

            </xsl:attribute>

                <xsl:text>[</xsl:text>

                    <xsl:text>+</xsl:text>

            <xsl:if test="./@cert='low'">

                <xsl:text>?</xsl:text>

            </xsl:if>

                <xsl:value-of select="(.)"/>

                <xsl:text>]</xsl:text>

        </xsl:element>

    </xsl:template>

<!-- 4.1 subst just display add and del elements as above 

    <xsl:template match="tei:subst">

        <xsl:apply-templates/>

    </xsl:template>

-->

    

<!-- 4.2 expan  -->

    <xsl:template match="tei:expan">

        <xsl:element name="span">

      <xsl:attribute name="class">xsl-abbrev</xsl:attribute>

      <xsl:attribute name="title">

          <xsl:value-of select="concat(./tei:abbr, ./tei:ex)"/>

      </xsl:attribute>

        <xsl:apply-templates select="./tei:abbr"/>

        </xsl:element>

    </xsl:template>

<!-- 4.3 chioce (a/b/c) -->

    <xsl:template match="tei:choice">

        <xsl:choose>

            <xsl:when test="count(./tei:unclear)&gt;1">

        <xsl:text>(</xsl:text>

        <xsl:for-each select="./tei:unclear">

            <xsl:value-of select="."/>

            <xsl:if test="not(position()=last())">

                <xsl:text>/</xsl:text>

        </xsl:if>    

        </xsl:for-each>

        <xsl:text>)</xsl:text>

            </xsl:when>

            <xsl:when test="./tei:sic and ./tei:corr">

                <xsl:element name="span">

                    <xsl:attribute name="class">xsl-sic-cor-edition</xsl:attribute>

                    <xsl:attribute name="title">

                        <xsl:value-of select="concat('inscription: ', ./tei:sic)"/>

                    </xsl:attribute>

                    <xsl:value-of select="./tei:corr"/>

                </xsl:element>

                <!-- for dip view -->

                <xsl:element name="span">

                    <xsl:attribute name="class">xsl-sic-dip</xsl:attribute>

                    <xsl:attribute name="title">

                        <xsl:value-of select="concat('correction: ', ./tei:corr)"/>

                    </xsl:attribute>

                    <xsl:value-of select="./tei:sic"/>

                </xsl:element>

            </xsl:when>

        </xsl:choose>

        <!-- suppress further calls to elements 

        <xsl:apply-templates/>

        -->

    </xsl:template>

    <!-- rarely used damaged , show as grey background -->

    <xsl:template match="tei:damage">

        <xsl:element name="span">

            <xsl:attribute name="class">xsl-damage</xsl:attribute>

        <xsl:apply-templates/>

        </xsl:element>

    </xsl:template>

 

    <!-- hide hyphens in dip view 

    <xsl:template match="text()">

        <xsl:call-template name="replace-string">

            <xsl:with-param name="text" select="."/>

            <xsl:with-param name="replace" select="'-'" />

            <xsl:with-param name="with" select="' '"/>

        </xsl:call-template>

    </xsl:template>

 -->

   

 <!-- hide spaces in diplomatic view 

    <xsl:template match="text()">

        <xsl:call-template name="replace-string">

            <xsl:with-param name="text" select="."/>

            <xsl:with-param name="replace" select="' '" />

            <xsl:with-param name="with" select="''"/>

        </xsl:call-template>

    </xsl:template>

    -->

    <xsl:template match="text()">

       <xsl:call-template name="get-letters">

           <xsl:with-param name="input" select="."/>

       </xsl:call-template>

  

 <!--         <xsl:choose>

            <xsl:when test="contains(.,'-')">

                <xsl:value-of select="substring-before(.,'-')"/>

                    <xsl:element name="span">

                        <xsl:attribute name="class">xsl-edition-only</xsl:attribute>

                        <xsl:text>-</xsl:text>                   

                    </xsl:element>

                <xsl:value-of select="substring-after(.,'-')"/>

            </xsl:when>

            <xsl:when test="contains(.,' ')">

                <xsl:value-of select="substring-before(.,'')"/>

                <xsl:element name="span">

                    <xsl:attribute name="class">xsl-edition-only</xsl:attribute>

                    <xsl:text> </xsl:text>                   

                </xsl:element>

                <xsl:value-of select="substring-after(.,'')"/>

            </xsl:when>

            <xsl:otherwise>

                <xsl:value-of select="."/>

            </xsl:otherwise>

        </xsl:choose> -->

    </xsl:template>

    

   <xsl:template name="para">

       <xsl:param name="inc" select="1"/>

       <xsl:param name="x" select=". + 1"/>

       <xsl:value-of select="concat('Paragraph ', $x + 1)"/>

   </xsl:template>

   

    <!-- this just sets the output for gap -->

    <xsl:template name="determine_char">

        <xsl:param name="reason" select="'#'"/>

        <xsl:param name="char"/>

        <xsl:param name="quantity" select="''"/>

        <xsl:param name="extent" select="''"/>

        <xsl:param name="unit" select="'character'"/>

        <xsl:param name="precision" select="''"/>

        <xsl:choose>

            <xsl:when test="$unit='character' and $precision='low' and $quantity=1">

                <xsl:value-of select="concat('?',$quantity, '')"/>

            </xsl:when>

            <xsl:when test="$unit='character' and $precision='low'">

                <xsl:value-of select="concat($char,'?',$quantity,$char)"/>

            </xsl:when>

            <xsl:when test="$unit='character' and $quantity='' and $extent='unknown'">

                <xsl:value-of select="concat($char,$char,$char)"/>

            </xsl:when>

            <xsl:when test="$unit='character' and $quantity=1">

                <xsl:value-of select="concat(' ',$char, ' ')"/>

            </xsl:when>

            <xsl:when test="$unit='character' and $quantity &gt;1">

                <xsl:value-of select="concat($char,$quantity,$char)"/>

            </xsl:when>

            <xsl:when test="$unit='line'">

                <xsl:call-template name="iterate">

                    <xsl:with-param name="i" select="1"/>

                    <xsl:with-param name="count" select="$quantity"/>

                    <xsl:with-param name="char_type">

                        <xsl:value-of select="concat('&lt;br /&gt;', $char,$char,$char)"/>

                    </xsl:with-param>

                </xsl:call-template>

            </xsl:when>

            <xsl:when test="string-length($unit)='0'">

                <xsl:value-of select="'...'"/>

            </xsl:when>

        </xsl:choose>

    </xsl:template>

    

    <!-- replace chars -->

    <xsl:template name="replace-string">

        <xsl:param name="text"/>

        <xsl:param name="replace"/>

        <xsl:param name="with"/>

        <xsl:choose>

            <xsl:when test="contains($text,$replace)">

                <xsl:value-of select="substring-before($text,$replace)"/>

                <xsl:value-of select="$with"/>

                <xsl:call-template name="replace-string">

                    <xsl:with-param name="text" select="substring-after($text,$replace)"/>

                    <xsl:with-param name="replace" select="$replace"/>

                    <xsl:with-param name="with" select="$with"/>

                </xsl:call-template>

            </xsl:when>

            <xsl:otherwise>

                <xsl:value-of select="$text"/>

            </xsl:otherwise>

        </xsl:choose>

      </xsl:template>

 

    <xsl:template name="get-letters">

        <xsl:param name="input"/>

        <xsl:if test="string-length($input)">

            

            <xsl:choose>

                <!-- DO NOT HIDE SPCES IN DIP VIEW ANYMORE 

                <xsl:when test="substring($input, 1, 1)=' '">

                    <xsl:element name="span">

                        <xsl:attribute name="class">xsl-edition-only</xsl:attribute>

                        <xsl:text> </xsl:text>                   

                    </xsl:element>

                </xsl:when>

                -->

                <xsl:when test="substring($input, 1, 1)= '-'">

                    <xsl:element name="span">

                        <xsl:attribute name="class">xsl-edition-only</xsl:attribute>

                        <xsl:text>-</xsl:text>                   

                    </xsl:element>

                </xsl:when>

                <xsl:otherwise>

                    <xsl:value-of select="substring($input, 1, 1)"/>

                </xsl:otherwise>    

            </xsl:choose>

            

            <xsl:call-template name="get-letters">

                <xsl:with-param name="input" select="substring($input, 2)"/>

            </xsl:call-template>

        </xsl:if>

    </xsl:template>

  

 

 <!-- seg-met-convertor translates metric symbols -->   

    <xsl:template name="seg-met-convert">

        <xsl:param name="meter"/>

        <xsl:if test="string-length($meter)">

            <xsl:choose>

                <xsl:when test="substring($meter,1,1) = '='">

                    <xsl:text>-</xsl:text>

                </xsl:when>

                <xsl:when test="substring($meter,1,1) = '.'">

                    <xsl:value-of select="$met_brev"/>

                </xsl:when>

                <xsl:when test="substring($meter,1,1) = '*'">

                    <xsl:value-of select="$short_over_long"/>

                </xsl:when>

                <xsl:otherwise>

                    <xsl:value-of select="substring($meter,1,1)"/>

                </xsl:otherwise>

            </xsl:choose>

            <xsl:call-template name="seg-met-convert">

               

                <xsl:with-param name="meter" select="substring($meter, 2)"/>

            </xsl:call-template>

        </xsl:if>

    </xsl:template>

</xsl:stylesheet>