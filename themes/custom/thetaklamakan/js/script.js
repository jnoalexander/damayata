
function redirect_pagging(page_url)
{
    var view_type = document.getElementById("view_type").value;
    window.location.href = page_url + '&view_type=' + view_type;

}
jQuery(document).ready(function () {

    if (jQuery('#grid-link').length > 0) {

        var view_type2 = document.getElementById("view_type").value;
        //jQuery("#grid-link").show();
        // jQuery("#list-link").hide();
        //jQuery(".block-list-view").hide();
        //jQuery(".block-grid-view").show();
        if (view_type2 == 'style-list') {
            jQuery(".search-results").removeClass('style-grid');
            jQuery(".search-results").addClass('style-list');
            jQuery(".block-list-view").show();
            jQuery(".block-grid-view").hide();

            jQuery("#grid-link").show();
            jQuery("#list-link").hide();


        } else {
            jQuery(".search-results").removeClass('style-list');
            jQuery(".search-results").addClass('style-grid');

            jQuery(".block-list-view").hide();
            jQuery(".block-grid-view").show();

            jQuery("#grid-link").hide();
            jQuery("#list-link").show();
        }

        jQuery("#grid-link").click(function () {

            jQuery("#grid-link").hide();
            jQuery("#list-link").show()
            jQuery(".block-list-view").hide();
            jQuery(".block-grid-view").show();
            jQuery(".search-results").removeClass('style-list');
            jQuery(".search-results").addClass('style-grid');
            jQuery("#view_type").val('style-grid');
            return false;
        });
        jQuery("#list-link").click(function () {


            jQuery("#grid-link").show();
            jQuery("#list-link").hide();
            jQuery(".block-list-view").show();
            jQuery(".block-grid-view").hide();
            jQuery(".search-results").removeClass('style-grid');
            jQuery(".search-results").addClass('style-list');
            jQuery("#view_type").val('style-list');
            return false;
        });
    }

    jQuery('[data-toggle="tooltip"]').tooltip();

    if (jQuery('.refine-button').length > 0) {
        jQuery('.refine-button').click(function () {
            jQuery(".refine-content").toggle();
            return false;
        });
    }

    /* Refine checkbox clicks*/
    attachClicktoRefineCheckbox('#type', '#checkboxModal');
    attachClicktoRefineCheckbox('#form', '#checkboxModal');
    attachClicktoRefineCheckbox('#institute_collection', '#checkboxModal');
    attachClicktoRefineCheckbox('#materials_techniques', '#checkboxModal');
    attachClicktoRefineCheckbox('#languages_scripts', '#checkboxModal');
    attachClicktoRefineCheckbox('#provenance', '#checkboxModal');
    attachClicktoRefineCheckbox('#archaeological_find_site', '#checkboxModal');
    attachClicktoRefineCheckbox('#subject_keyword', '#checkboxModal');
    attachClicktoRefineCheckbox('#date', '#checkboxModal');
    /* Refine checkbox clicks end*/


    /*Discover page*/
    if (jQuery('.build-search-block-form .btn-close').length > 0) {
        jQuery('.build-search-block-form .btn-close').click(function () {
            jQuery(this).parent().parent().remove();
            searchbuildRowCount--;
            return false;
        });
    }
    var searchbuildRowCount = 3;
    if (jQuery('.build-search-block-form #add-search-element').length > 0) {
        jQuery('.build-search-block-form #add-search-element').click(function () {
            searchbuildRowCount++;
            content = jQuery('.build-search-block-form .sample-content').html().replace(/\{num}/g, searchbuildRowCount);
            content = '<li class="clearfix">' + content + '</li>';
            jQuery('.build-search-block-form .search-list').append(content);
            jQuery('.build-search-block-form .btn-close').click(function () {
                jQuery(this).parent().parent().remove();
                searchbuildRowCount--;
                return false;
            });
            return false;
        });
    }



    try {
        var pagging = jQuery('nav.pager').html();

        jQuery('#top_pagging').html(pagging);

    } catch (e) {
    }

});

//Refine checkbox attach clicks.
function attachClicktoRefineCheckbox(checkboxId, modalId) {
    if (jQuery('.refine-content ' + checkboxId).length > 0) {
        jQuery('.refine-content ' + checkboxId).click(function () {
            if (jQuery('.refine-content ' + checkboxId + ':checked').length > 0) {//the click is checking the box
                jQuery(modalId).modal('show');
                jQuery(this).parent().parent().css({"background-color": "#f2f2f2"});
            } else {//the click is un-checking the box
                jQuery(this).parent().parent().css({"background-color": "transparent"});
            }
        });
    }
}

/*For help/faq page accordian.*/
jQuery(function () {
    jQuery("#accordion .panel").each(function () {
        jQuery(this).on('hidden.bs.collapse', function () {
            jQuery(this).find('.panel-sub-title').show();
        }).on('show.bs.collapse', function () {
            jQuery(this).find('.panel-sub-title').hide();
        });
    });
});

jQuery(document).ready(function () {

    jQuery("#inc_search").click(function () {
        var value = document.getElementById('edit-keys').value;
        var url = document.getElementById('base_insecure_url').value;
        if (value != "") {
            window.location = url + "/search/inscription?keys=" + value + "&f[0]=type:Inscription";
        } else {
            window.location = url + "/search/inscription?keys=Inscription&f[0]=type:Inscription";
        }

    });

    jQuery("#all_search").click(function () {
        var value = document.getElementById('edit-keys').value;
        var url = document.getElementById('base_insecure_url').value;
        var is_flag = document.getElementById('is_flag').value;
        if (is_flag != "") {
            if (is_flag == 'articals') {
                window.location = url + "/search/articals?keys=" + value + "&f[0]=type:Inscription";
                exit;
            } else if (is_flag == 'result') {
                window.location = url + "/search/result?keys=" + value + "&f[0]=type:Inscription";
                exit;
            } else if (is_flag == 'inscription') {
                window.location = url + "/search/inscription?keys=" + value + "&f[0]=type:Inscription";
                exit;
            }
        } else {
            window.location = url + "/search/result?keys=" + value + "&f[0]=type:Inscription";
        }

    });

});
